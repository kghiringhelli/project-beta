from django.urls import path
from .views import list_techs, delete_technician, list_appointments, delete_appointment, cancel_appointment, finish_appointment

urlpatterns = [
    path("technicians/", list_techs, name="list_techs"),
    path("technicians/<int:id>", delete_technician, name="delete_technician"),
    path("appointments/", list_appointments, name="list_appointments"),
    path("appointments/<int:id>/", delete_appointment, name="delete_appointment"),
    path("appointments/<int:id>/cancel", cancel_appointment, name="cancel_appointment"),
    path("appointments/<int:id>/finish", finish_appointment, name="finish_appointment"),
]

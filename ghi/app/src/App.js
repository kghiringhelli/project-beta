import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import Manufacturers from './components/Manufacturers';
import CreateManufacturer from './components/CreateManufacturer';
import Models from './components/Models';
import CreateModel from './components/CreateModel';
import Automobiles from './components/Automobiles';
import CreateAutomobile from './components/CreateAutomobile';
import Salespeople from './components/Salespeople';
import AddSalesperson from './components/AddSalesperson';
import Customers from './components/Customers';
import AddCustomer from './components/AddCustomer';
import Sales from './components/Sales';
import AddSale from './components/AddSale';
import Technicians from './components/Technicians';
import AddTechnician from './components/AddTechnician';
import ServiceAppointments from './components/ServiceAppointments';
import CreateServiceAppointment from './components/CreateServiceAppointment';
import ServiceHistory from './components/ServiceHistory';
import withNotification from './hocs/withNotification';

const WrappedCreateModel = withNotification(CreateModel);
const WrappedCreateManufacturer = withNotification(CreateManufacturer);
const WrappedAddCustomer = withNotification(AddCustomer);
const WrappedAddSale = withNotification(AddSale);
const WrappedAddTech = withNotification(AddTechnician);
const WrappedCreateServiceAppointment = withNotification(CreateServiceAppointment);
const WrappedCreateAutomobile = withNotification(CreateAutomobile);
const WrappedModels = withNotification(Models);


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/manufacturers" element={<Manufacturers />} />
          <Route path="/create-manufacturer" element={<WrappedCreateManufacturer />} />
          <Route path="/models" element={<WrappedModels />} />
          <Route path="/create-model" element={<WrappedCreateModel />} />
          <Route path="/automobiles" element={<Automobiles />} />
          <Route path="/create-automobile" element={<WrappedCreateAutomobile />} />
          <Route path="/salespeople" element={<Salespeople />} />
          <Route path="/add-salesperson" element={<AddSalesperson />} />
          <Route path="/customers" element={<Customers />} />
          <Route path="/add-customer" element={<WrappedAddCustomer />} />
          <Route path="/sales" element={<Sales />} />
          <Route path="/add-sale" element={<WrappedAddSale />} />
          <Route path="/technicians" element={<Technicians />} />
          <Route path="/add-technician" element={<WrappedAddTech />} />
          <Route path="/service-appointments" element={<ServiceAppointments />} />
          <Route path="/create-service-appointment" element={<WrappedCreateServiceAppointment />} />
          <Route path="/service-history" element={<ServiceHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

import React, { useState } from 'react';
import withNotification from '../hocs/withNotification';

function CreateManufacturer({ showNotification }) {
  const [name, setName] = useState('');


  const handleSubmit = async (event) => {
    event.preventDefault();

    // Validate the Name length
    if (name.length === 0 || name.length > 100) {
      showNotification('Name should not be empty or exceed 100 characters', 'danger');
      return;
    }

    const data = {
      name: name
    };
    const url = 'http://localhost:8100/api/manufacturers/';
    const config = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    try {
      const response = await fetch(url, config);
      if (response.ok) {
        const responseData = await response.json();
        setName('');
        showNotification('Manufacturer created successfully', 'success','/manufacturers');
      } else {
        showNotification('Error creating manufacturer', 'danger');
      }
    } catch (error) {
      showNotification(`Error creating manufacturer: ${error}`, 'danger');
    }
  };

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a manufacturer</h1>
          <form onSubmit={handleSubmit} id="create-manufacturer-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleNameChange}
                value={name}
                placeholder="Name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="name">Name</label>
            </div>
            <button className="btn btn-primary">Create Manufacturer</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default withNotification(CreateManufacturer);

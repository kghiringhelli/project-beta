import React, { useState, useEffect } from 'react';

function Salespeople() {
  const [salespeople, setSalespeople] = useState([]);

  const fetchSalespeople = async () => {
    const url = 'http://localhost:8090/api/salespeople/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
    }
  }

  useEffect(() => {
    fetchSalespeople();
  }, []);

  return (
    <div className="container">
      <h1>Salespeople</h1>
      <hr />
      <table className="table table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Employee ID</th>
          </tr>
        </thead>
        <tbody>
          {salespeople.map((salesperson) => (
            <tr key={salesperson.id}>
              <td>{salesperson.id}</td>
              <td>{salesperson.first_name} {salesperson.last_name}</td>
              <td>{salesperson.employee_id}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default Salespeople;

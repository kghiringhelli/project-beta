from django.shortcuts import render
from .models import Technician, AutomobileVO, Appointment
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin"
    ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "vip",
        "status",
        "vin",
        "customer",
        "technician"
    ]
    encoders={"technician": TechnicianEncoder()}

@require_http_methods(["GET", "POST"])
def list_techs(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder
        )
    else:
        try:
            content = json.loads(request.body)
        except:
            return JsonResponse(
                {"Error": "Could not create technician"},
                status=400,
            )
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def delete_technician(request, id):
    if Technician.objects.filter(id=id):
        count, _ = Technician.objects.filter(id=id).delete()
        return JsonResponse({"Deleted": count > 0})
    else:
        return JsonResponse(
            {"Error": "Could not delete technician because they do not exist"},
            status=400,
        )

@require_http_methods(["GET", "POST"])
def list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            try:
                if AutomobileVO.objects.get(vin=content["vin"]):
                    content["vip"] = True
                    appointment = Appointment.objects.create(**content)
            except AutomobileVO.DoesNotExist:
                content["vip"]= False
                appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False
            )
        except:
            return JsonResponse(
                {"Error": "Could not create appointment"},
                status=400,
            )


@require_http_methods(["DELETE"])
def delete_appointment(request, id):
    if Appointment.objects.filter(id=id):
        count, _ = Appointment.objects.filter(id=id).delete()
        return JsonResponse({"Deleted": count > 0})
    else:
        return JsonResponse(
            {"Error": "Could not delete appointment because it does not exist"},
            status=400,
        )
@require_http_methods(["PUT"])
def cancel_appointment(request, id):
    if Appointment.objects.filter(id=id):
        appointment = Appointment.objects.get(id=id)
        appointment.status = "Canceled"
        appointment.save()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )
    else:
        return JsonResponse(
            {"Error": "Could not cancel appointment because it does not exist"},
            status=400,
        )

@require_http_methods(["PUT"])
def finish_appointment(request, id):
    if Appointment.objects.filter(id=id):
        appointment = Appointment.objects.get(id=id)
        appointment.status = "Finished"
        appointment.save()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )
    else:
        return JsonResponse(
            {"Error": "Could not finish appointment because it does not exist"},
            status=400,
        )

import React, { useState, useEffect } from 'react';

function Notification({ message, type, duration = 3000 }) {
  const [visible, setVisible] = useState(true);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setVisible(false);
    }, duration);

    return () => clearTimeout(timeout);
  }, [duration]);

  const alertClass = type === 'success' ? 'alert-success' : 'alert-danger';

  return (
    visible && (
      <div className={`alert ${alertClass} fixed-bottom mb-3 mx-auto w-50`} role="alert">
        {message}
      </div>
    )
  );
}

export default Notification;

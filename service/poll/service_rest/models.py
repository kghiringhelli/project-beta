from django.db import models

# Create your models here.

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=255)

class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.TextField()
    CREATED = "Created"
    CANCELED = "Canceled"
    FINISHED = "Finished"
    STATUS_CHOICES = ((CREATED, "Created"), (CANCELED, "Canceled"), (FINISHED, "Finished"))
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default=CREATED)
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=200)
    technician = models.ForeignKey(
        Technician,
        related_name="appointment",
        null=True,
        on_delete=models.CASCADE
    )

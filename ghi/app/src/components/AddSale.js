import React, { useState, useEffect } from 'react';
import withNotification from '../hocs/withNotification';

function AddSale( {showNotification} ) {
  const [automobiles, setAutomobiles] = useState([]);
  const [salespersons, setSalespersons] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [price, setPrice] = useState('');
  const [selectedAutomobile, setSelectedAutomobile] = useState('');
  const [selectedSalesperson, setSelectedSalesperson] = useState('');
  const [selectedCustomer, setSelectedCustomer] = useState('');

  const fetchAutomobiles = async () => {
    const url = 'http://localhost:8100/api/automobiles/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      const availableAutomobiles = data.autos.filter(
        (automobile) => !automobile.sold
      );
      setAutomobiles(availableAutomobiles);
    } else {
      showNotification('Error fetching automobiles.', 'danger');
    }
  };

  const fetchSalespersons = async () => {
    const url = 'http://localhost:8090/api/salespeople/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSalespersons(data.salespeople);
    }else {
      showNotification('Error fetching sales people.', 'danger');
    }
  };

  const fetchCustomers = async () => {
    const url = 'http://localhost:8090/api/customers/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }else {
      showNotification('Error fetching customers.', 'danger');
    }
  };

  useEffect(() => {
    fetchAutomobiles();
    fetchSalespersons();
    fetchCustomers();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    if(selectedAutomobile === '') {
      showNotification('Automobile is required.', 'danger');
      return;
    }

    if(selectedSalesperson === '') {
      showNotification('Salesperson is required.', 'danger');
      return;
    }

    if(selectedCustomer === '') {
      showNotification('Customer is required.', 'danger');
      return;
    }

    if (price === '') {
      showNotification('Price is required and must be a valid currency value.', 'danger');
      return;
    }

    const data = {
      vin: selectedAutomobile,
      price: price,
      salesperson_id: selectedSalesperson,
      customer_id: selectedCustomer,
    }

    const url = 'http://localhost:8090/api/sales/';

    const config = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    }

    try{
      const response = await fetch(url, config);
      if (response.ok) {
        setAutomobiles([]);
        setCustomers([]);
        setSalespersons([]);
        setPrice('');
        showNotification('Sale created successfully!', 'success', '/sales');
      } else {
        showNotification(`Error creating sale`, 'danger');
      }
    } catch (error) {
      showNotification(`Error creating sale: ${error}`, 'danger');
    }

  };

  return (
    <div className="container">
      <h1>Add Sale</h1>
      <hr/>
      <form onSubmit={handleSubmit}>
        <div className="row">
          <div className="col-md-6 mb-3">
            <label htmlFor="automobile">Automobile</label>
            <select
              className="form-control"
              id="automobile"
              value={selectedAutomobile}
              onChange={(e) => setSelectedAutomobile(e.target.value)}
            >
              <option value="">Select an automobile</option>
              {automobiles.map((automobile) => (
                <option key={automobile.vin} value={automobile.vin}>
                  {automobile.vin} - {automobile.model.name}
                </option>
              ))}
            </select>
          </div>
          <div className="col-md-6 mb-3">
            <label htmlFor="salesperson">Salesperson</label>
            <select
              className="form-control"
              id="salesperson"
              value={selectedSalesperson}
              onChange={(e) => setSelectedSalesperson(e.target.value)}
            >
              <option value="">Select a salesperson</option>
              {salespersons.map((salesperson) => (
                <option key={salesperson.id} value={salesperson.id}>
                  {salesperson.first_name} {salesperson.last_name}
                </option>
              ))}
            </select>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6 mb-3">
            <label htmlFor="customer">Customer</label>
            <select
              className="form-control"
              id="customer"
              value={selectedCustomer}
              onChange={(e) => setSelectedCustomer(e.target.value)}
            >
              <option value="">Select a customer</option>
              {customers.map((customer) => (
                <option key={customer.id} value={customer.id}>
                  {customer.first_name} {customer.last_name}
                </option>
              ))}
            </select>
          </div>
          <div className="col-md-6 mb-3">
            <label htmlFor="price">Price</label>
            <input
              type="number"
              className="form-control"
              id="price"
              value={price}
              onChange={(e) => setPrice(e.target.value)}
            />
          </div>
        </div>
        <button type="submit" className="btn btn-primary">
          Add Sale
        </button>
      </form>
    </div>
  );
}


export default withNotification(AddSale);

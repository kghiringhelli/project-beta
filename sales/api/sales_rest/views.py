from django.shortcuts import render
import requests

from django.http import JsonResponse, HttpResponse, Http404
from django.views.decorators.http import require_http_methods
import json

from decimal import Decimal


from common.json import ModelEncoder

from .models import AutomobileVO, Customer,  Salesperson, Sale

class SalesPersonSalesEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]

    def default(self, obj):
        if isinstance(obj, self.model):
            salesperson_data = super().default(obj)
            salesperson_data['sales'] = []

            for sale in obj.sales.all():
                sale_data = {
                    "id": sale.id,
                    "automobile_vin": sale.automobile.vin,
                    "customer": {
                        "id": sale.customer.id,
                        "first_name": sale.customer.first_name,
                        "last_name": sale.customer.last_name,
                        "address": sale.customer.address,
                    },
                    "price": str(sale.price),
                }
                salesperson_data['sales'].append(sale_data)

            return salesperson_data

        return super().default(obj)

class CustomerSalesEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
    ]

    def default(self, obj):
        if isinstance(obj, self.model):
            customer_data = super().default(obj)
            customer_data['sales'] = []

            for sale in obj.sales.all():
                sale_data = {
                    "id": sale.id,
                    "automobile_vin": sale.automobile.vin,
                    "salesperson": {
                        "id": sale.salesperson.id,
                        "first_name": sale.salesperson.first_name,
                        "last_name": sale.salesperson.last_name,
                        "employee_id": sale.salesperson.employee_id,
                    },
                    "price": str(sale.price),
                }
                customer_data['sales'].append(sale_data)

            return customer_data

        return super().default(obj)

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
    ]

class SalespeopleListEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]

class SalesListEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "price",
    ]

    def default(self, obj):
        if isinstance(obj, self.model):
            sale_data = super().default(obj)

            # Include the customer information
            sale_data["customer"] = {
                "id": obj.customer.id,
                "first_name": obj.customer.first_name,
                "last_name": obj.customer.last_name,
                "address": obj.customer.address,
            }

            # Include the salesperson information
            sale_data["salesperson"] = {
                "id": obj.salesperson.id,
                "first_name": obj.salesperson.first_name,
                "last_name": obj.salesperson.last_name,
                "employee_id": obj.salesperson.employee_id,
            }

            # Include the automobile VIN
            sale_data["vin"] = obj.automobile.vin

            return sale_data
        elif isinstance(obj, Decimal):
            return str(obj)

        return super().default(obj)


@require_http_methods(["GET", "DELETE"])
def api_customer_details(request, customer_id):
    """
    Returns the details of a customer as JSON or delete the customer.
    The customer is identifed by its primary key (customer_id)

    GET: return the customer requested and its underlying sales data

    JSON Structure:
    {
        "id": 2,
        "first_name": "Jane",
        "last_name": "Smith",
        "address": "123 Main St",
        "sales": []
    }

    DELETE: deletes the customer

    """
    try:
        customer = Customer.objects.get(id=customer_id)
    except Customer.DoesNotExist:
        raise Http404("Customer not found")


    if request.method == "GET":
        customer_data  = json.loads(json.dumps(customer, cls=CustomerSalesEncoder))
        return JsonResponse(customer_data, safe=False)
    elif request.method == "DELETE" :
        customer.delete()
        return HttpResponse(status=200)

@require_http_methods(["GET","POST"])
def api_list_customers(request):
    """
    This view serves tgwo purposes:

    List all customers or create a new customer.

    GET: Returns a JSON array of all customers.

    JSON Structure:
    {
        "customers" : [
            {
                "id" : "1",
                "first_name": "Jane",
                "last_name": "Smith",
                "address": "123 Main St"
            },
            ...
        ]
    }

    POST: Create a new customer.

    JSON Structure:
    {
        "first_name": "Jane",
        "last_name": "Smith",
        "address": "123 Main St"
    }


    """

    if request.method == "GET":
        customers = Customer.objects.all()
        customers_data = json.loads(json.dumps(list(customers), cls=CustomerListEncoder))
        return JsonResponse({"customers": customers_data}, safe=False)
    else:
        content = json.loads(request.body)
        try:
            first_name = content["first_name"]
            last_name = content["last_name"]
            address = content["address"]
            customer = Customer.objects.create(
                first_name = first_name,
                last_name = last_name,
                address = address,

            )
            customer.save()
            return JsonResponse({"message": "Customer created successfully"}, status = 200)
        except KeyError as e:
            return HttpResponse(f"Missing required field: {str(e)}", status=400)

@require_http_methods(["GET", "DELETE"])
def api_salesperson_details(request, salesperson_id):
    """
    Returns the details of a salesperson as JSON or delete the salesperson.
    The salesperson is identifed by its primary key (salesperson_id)

    GET: return the salesperson requested and its underlying sales data

    JSON Structure:
    {
        "id": 2,
        "first_name": "Jane",
        "last_name": "Smith",
        "employee_id": "123,
        "sales": []
    }

    DELETE: deletes the salesperson

    """
    try:
        salesperson = Salesperson.objects.get(id=salesperson_id)
    except Salesperson.DoesNotExist:
        raise Http404("Salesperson not found")


    if request.method == "GET":
        salesperson_data  = json.loads(json.dumps(salesperson, cls=SalesPersonSalesEncoder))
        return JsonResponse(salesperson_data, safe=False)
    elif request.method == "DELETE" :
        salesperson.delete()
        return HttpResponse(status=200)

@require_http_methods(["GET","POST"])
def api_list_salespeople(request):
    """
    This view serves two purposes:

    List all salespeople or create a new salesperson.

    GET: Returns a JSON array of all salespeople.

    JSON Structure:
    {
        "salespeople" : [
            {
                "id" : "1",
                "first_name": "Jane",
                "last_name": "Smith",
                "employee_id": "123"
            },
            ...
        ]
    }

    POST: Create a new salesperson.

    JSON Structure:
    {
        "first_name": "Jane",
        "last_name": "Smith",
        "employee_id": "123"
    }


    """

    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        salespeople_data = json.loads(json.dumps(list(salespeople), cls=SalespeopleListEncoder))
        return JsonResponse({"salespeople": salespeople_data}, safe=False)
    else:
        content = json.loads(request.body)
        try:
            first_name = content["first_name"]
            last_name = content["last_name"]
            employee_id = content["employee_id"]
            customer = Salesperson.objects.create(
                first_name = first_name,
                last_name = last_name,
                employee_id = employee_id,

            )
            customer.save()
            return JsonResponse({"message": "Salesperson created successfully"}, status = 200)
        except KeyError as e:
            return HttpResponse(f"Missing required field: {str(e)}", status=400)

@require_http_methods(["GET", "DELETE"])
def api_sale_details(request, sale_id):
    """
    Returns the details of a sale as JSON or delete the sale.
    The sale is identifed by its primary key (sale_id)

    GET: return the sale requested and its underlying sales data

    JSON Structure:
    {
                "id" : "1",
                "price": "10000.00",
                "vin" : "12345",
                "salesperson": [
                    "id" : "1",
                    "first_name" : "Jane",
                    "last_name" : "Smith",
                    "employee_id" : "123"
                ]
                "customer" : [
                    "id" : "2",
                    "first_name" : "Fred",
                    "last_name" : "Smith",
                    "address'" : "123 N South St, Anoka, MN 55110"
                ]

            }

    DELETE: deletes the sale

    """
    try:
        sale = Sale.objects.get(id=sale_id)
    except Sale.DoesNotExist:
        raise Http404("Sale not found")


    if request.method == "GET":
        sale_data  = json.loads(json.dumps(sale, cls=SalesListEncoder))
        return JsonResponse(sale_data, safe=False)
    elif request.method == "DELETE" :
        sale.delete()
        return HttpResponse(status=200)

@require_http_methods(["GET","POST"])
def api_list_sales(request):
    """
    This view serves two purposes:

    List all sales or create a new sale.

    GET: Returns a JSON array of all sales.

    JSON Structure:
    {
        "sales" : [
            {
                "id" : "1",
                "price": "10000.00",
                "vin" : "12345",
                "salesperson": [
                    "id" : "1",
                    "first_name" : "Jane",
                    "last_name" : "Smith",
                    "employee_id" : "123"
                ]
                "customer" : [
                    "id" : "2",
                    "first_name" : "Fred",
                    "last_name" : "Smith",
                    "address'" : "123 N South St, Anoka, MN 55110"
                ]

            },
            ...
        ]
    }

    POST: Create a new sale.

    JSON Structure:
    {
        "vin": "12345",
        "price": "10000.00",
        "salesperson_id": 1,
        "customer_id": 2
    }


    """

    if request.method == "GET":
        sales = Sale.objects.all()
        sales_data = json.loads(json.dumps(list(sales), cls=SalesListEncoder))
        return JsonResponse({"sales": sales_data}, safe=False)
    else:
        content = json.loads(request.body)
        try:
            vin = content["vin"]
            price = content["price"]
            salesperson_id = content["salesperson_id"]
            customer_id = content["customer_id"]

            automobile = AutomobileVO.objects.get(vin=vin)
            salesperson = Salesperson.objects.get(id=salesperson_id)
            customer = Customer.objects.get(id=customer_id)

            sale = Sale.objects.create(
                automobile=automobile,
                salesperson=salesperson,
                customer=customer,
                price=price,
            )
            sale.save()

            # Update the sold field in the Automobile model using a PUT request to the inventory API
            inventory_api_url = f"http://project-beta-inventory-api-1:8000//api/automobiles/{vin}/"
            response = requests.put(
                inventory_api_url,
                json={"sold": True},
                headers={"Content-Type": "application/json"},
            )

            return JsonResponse({"message": "Sale created successfully"}, status=201)
        except KeyError as e:
            return HttpResponse(f"Missing required field: {str(e)}", status=400)
        except (AutomobileVO.DoesNotExist, Salesperson.DoesNotExist, Customer.DoesNotExist) as e:
            return HttpResponse(str(e), status=404)

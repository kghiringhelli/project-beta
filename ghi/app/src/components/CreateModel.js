import React, { useState, useEffect } from 'react';
import withNotification from '../hocs/withNotification';

function CreateModel({ showNotification }) {
  const [name, setName] = useState('');
  const [pictureUrl, setPictureUrl] = useState('');
  const [manufacturerId, setManufacturerId] = useState('');
  const [manufacturers, setManufacturers] = useState([]);

  useEffect(() => {
    const fetchManufacturers = async () => {
      try {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        const data = await response.json();
        const sortedManufacturers = data.manufacturers.sort((a, b) =>
          a.name.localeCompare(b.name)
        );
        setManufacturers(sortedManufacturers);
      } catch (error) {
        showNotification(`Error fetching manufacturers: ${error}`, 'danger');
      }
    };

    fetchManufacturers();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    if (manufacturerId === '') {
      showNotification('Manufacturer is required.', 'danger');
      return;
    }

    // Validate the Name length
    if (name.length === 0 || name.length > 100) {
      showNotification('Name should not be empty or exceed 100 characters', 'danger');
      return;
    }

    // Validate the URL length
    if (pictureUrl.length === 0 || pictureUrl.length > 200) {
      showNotification('URL should not be empty or exceed 200 characters', 'danger');
      return;
    }

    const data = {
      name: name,
      picture_url: pictureUrl,
      manufacturer_id: parseInt(manufacturerId, 10),
    };

    const url = 'http://localhost:8100/api/models/';
    const config = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const response = await fetch(url, config);
      if (response.ok) {
        const responseData = await response.json();
        setName('');
        setPictureUrl('');
        setManufacturerId('');
        showNotification('Model created successfully!', 'success','/models');
      } else {
        showNotification('Error creating model.', 'danger');
      }
    } catch (error) {
      showNotification(`Error creating model: ${error}`, 'danger');
    }
  };

  return (
    <div>
      <h1 className="mb-4">Create Vehicle Model</h1>
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label htmlFor="name" className="form-label">
            Name:
          </label>
          <input
            type="text"
            className="form-control"
            id="name"
            value={name}
            onChange={(event) => setName(event.target.value)}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="pictureUrl" className="form-label">
            Picture URL:
          </label>
          <input
            type="text"
            className="form-control"
            id="pictureUrl"
            value={pictureUrl}
            onChange={(event) => setPictureUrl(event.target.value)}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="manufacturer" className="form-label">
            Manufacturer:
          </label>
          <select
            className="form-select"
            id="manufacturer"
            value={manufacturerId}
            onChange={(event) => setManufacturerId(event.target.value)}
          >
            <option value="">Select a manufacturer</option>
            {manufacturers.map((manufacturer) => (
              <option key={manufacturer.id} value={manufacturer.id}>
                {manufacturer.name}
              </option>
            ))}
          </select>
        </div>
        <button type="submit" className="btn btn-primary">
          Create Model
        </button>
      </form>
    </div>
  );

}

export default withNotification(CreateModel);

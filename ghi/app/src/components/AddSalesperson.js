import React, { useState } from 'react';
import { useNavigate  } from 'react-router-dom';
import withNotification from '../hocs/withNotification';

function AddSalesperson({ showNotification }) {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [employeeId, setEmployeeId] = useState('');
  const navigate = useNavigate();

  const handleSubmit = async (event) => {
    event.preventDefault();

    //Validate Firstname
    if (firstName.length === 0 || firstName.length > 255) {
      showNotification('First name should not be empty or exceed 255 characters', 'danger');
      return;
    }

    //Validate Lastname
    if (lastName.length === 0 || lastName.length > 255) {
      showNotification('Last name should not be empty or exceed 255 characters', 'danger');
      return;
    }

    //Validate employee id
    if (employeeId.length === 0 || employeeId.length > 255) {
      showNotification('Employee Id should not be empty or exceed 255 characters', 'danger');
      return;
    }

    const data = {
      first_name: firstName,
      last_name: lastName,
      employee_id: employeeId,
    }

    const url = 'http://localhost:8090/api/salespeople/';
    const config = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    };

    try{
      const response = await fetch(url, config );
      if (response.ok) {
        setFirstName('');
        setLastName('');
        setEmployeeId('');
        showNotification('Salesperson added successfully!', 'success', '/salespeople');
      } else {
        showNotification('Error adding salesperson', 'danger');
      }
    } catch (error) {
      showNotification(`Error creating salesperson: ${error}`, 'danger');
    }


  };

  return (
    <div className="container">
      <h1>Add Salesperson</h1>
      <form onSubmit={handleSubmit}>
        <div className="row">
          <div className="col-md-6 mb-3">
            <label htmlFor="firstName">First Name</label>
            <input
              type="text"
              className="form-control"
              id="firstName"
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
            />
          </div>
          <div className="col-md-6 mb-3">
            <label htmlFor="lastName">Last Name</label>
            <input
              type="text"
              className="form-control"
              id="lastName"
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-12 mb-3">
            <label htmlFor="employeeId">Employee ID</label>
            <input
              type="text"
              className="form-control"
              id="employeeId"
              value={employeeId}
              onChange={(e) => setEmployeeId(e.target.value)}
            />
          </div>
        </div>
        <button type="submit" className="btn btn-primary">
          Add Salesperson
        </button>
      </form>
    </div>
  );
  }

  export default withNotification(AddSalesperson);

import React, {useState, useEffect} from 'react';


function ServiceAppointments() {

  const [appointments, setAppointments] = useState([]);

  const getAppointments = async () => {
    const url = 'http://localhost:8080/api/appointments/'
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }
  }

  useEffect(() => {
    getAppointments();
}, []);


  const handleFinish = async (event) => {
    const url = `http://localhost:8080${event}finish`
    const fetchConfig = {
      method: "put",
    }
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      getAppointments();
    }
  }


  const handleCancel = async (event) => {
    const url = `http://localhost:8080${event}cancel`
    const fetchConfig = {
      method: "put",
    }
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      getAppointments();
    }
  }

  return (
    <div>
      <h1>Appointments</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>VIP?</th>
            <th>Customer</th>
            <th>Booked</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Update</th>
          </tr>
        </thead>
        <tbody>
          {appointments?.map((appt, index) => {
            if (appt.status === "Created") {
            return (
              <tr key={appt.vin + index}>
                <td>{appt.vin}</td>
                <td>{appt.vip ? 'Yes' : 'No'}</td>
                <td>{appt.customer}</td>
                <td>{new Date(appt.date_time).toLocaleDateString("en-US")} {new Date(appt.date_time).toLocaleTimeString("en-US", { hour: "2-digit", minute: "2-digit" })}</td>
                <td>{appt.technician.first_name} {appt.technician.last_name}</td>
                <td>{appt.reason}</td>
                <td>
                  <div>
                    <button onClick={() => handleFinish(appt.href)} className="btn btn-success">Finish</button>
                  </div>
                  <div>
                    <button onClick={() => handleCancel(appt.href)} className="btn btn-danger">Cancel</button>
                  </div>
                </td>
              </tr>
            )
          }})}
        </tbody>
      </table>
    </div>
  );
}

export default ServiceAppointments;

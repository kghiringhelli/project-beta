import React, {useState, useEffect} from 'react';

function Technicians() {

  const [technicians, setTechs] = useState([]);

  const getTechs = async () => {
    const techUrl = 'http://localhost:8080/api/technicians/';
    const response = await fetch(techUrl);
    if (response.ok) {
      const data = await response.json();

      setTechs(data.technicians);
    }
  }

  useEffect(() => {
    getTechs();
}, []);

  return (
    <div>
      <h1>Technicians</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {technicians?.map((tech, index) => {
            return (
              <tr key={tech.employee_id + index}>
                <td>{tech.employee_id}</td>
                <td>{tech.first_name} {tech.last_name}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  );
}

export default Technicians;

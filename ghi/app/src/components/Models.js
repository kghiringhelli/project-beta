import React, { useState, useEffect } from 'react';
import withNotification from '../hocs/withNotification';

function Models( {showNotification} ) {
  const [models, setModels] = useState([]);

  useEffect(() => {
    const fetchModels = async () => {

        const response = await fetch('http://localhost:8100/api/models');
        if (response.ok) {
          const data = await response.json();
          setModels(data.models);
        } else {
          showNotification('Error fetching models:', 'danger')
        }

    };

    fetchModels();
  }, []);

  return (
    <div>
      <h1 className="mb-4">Vehicle Models</h1>
      <div className="row">
        {models.map((model) => (
          <div key={model.id} className="col-md-4 col-sm-6 mb-4">
            <div className="card h-100">
              <img src={model.picture_url} className="card-img-top" alt={model.name} />
              <div className="card-body">
                <h5 className="card-title">{model.name}</h5>
                <p className="card-text">
                  Manufacturer: {model.manufacturer.name}
                </p>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default Models;

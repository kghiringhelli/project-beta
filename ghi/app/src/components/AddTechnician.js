import React, { useState } from "react"
import withNotification from '../hocs/withNotification';


function AddTechnician({showNotification}) {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [employeeID, setEmployeeID] = useState('');


  const handleSubmit = async (event) => {
    event.preventDefault();

    if (firstName.length === 0 || firstName.length > 100) {
      showNotification('First name should not be empty or exceed 100 characters', 'danger');
      return;
    }

    if (lastName.length === 0 || lastName.length > 100) {
      showNotification('Last name should not be empty or exceed 100 characters', 'danger');
      return;
    }

    if (employeeID.length === 0 || employeeID.length > 255) {
      showNotification('Employee ID should not be empty or exceed 255 characters', 'danger');
      return;
    }

    const data = {};
    data.first_name = firstName;
    data.last_name = lastName;
    data.employee_id = employeeID;

    const url = 'http://localhost:8080/api/technicians/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
    },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const newTech = await response.json();
    setFirstName('');
    setLastName('');
    setEmployeeID('');
    showNotification('Technician added successfully', 'success', '/technicians')
    } else {
      showNotification('Error adding technician','danger');
    }
  };

  const handleFirstNameChange = (event) => {
    const value = event.target.value;
    setFirstName(value);
  };

  const handleLastNameChange = (event) => {
    const value = event.target.value;
    setLastName(value);
  };

  const handleIDChange = (event) => {
    const value = event.target.value;
    setEmployeeID(value);
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Add a Technician</h1>
        <form onSubmit={handleSubmit} id="create-tech-form">
        <div className="form-floating mb-3">
          <input onChange={handleFirstNameChange} value={firstName}placeholder="First Name" required type="text" name="firstName" id="firstName" className="form-control" />
            <label htmlFor="firstName">First Name</label>
        </div>
        <div className="form-floating mb-3">
          <input onChange={handleLastNameChange} value={lastName}placeholder="Last Name" required type="text" name="lastName" id="lastName" className="form-control" />
            <label htmlFor="lastName">Last Name</label>
        </div>
        <div className="form-floating mb-3">
          <input onChange={handleIDChange} value={employeeID}placeholder="Employee ID" required type="text" name="employeeID" id="employeeID" className="form-control" />
            <label htmlFor="employeeID">Employee ID</label>
        </div>
        <button className="btn btn-primary">Create Technician</button>
        </form>
      </div>
      </div>
    </div>
  );
}

export default withNotification(AddTechnician);

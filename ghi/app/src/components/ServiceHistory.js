import React, {useState, useEffect} from 'react';
import {useSearchParams} from 'react-router-dom';

function ServiceHistory() {
  const [services, setServices] = useState([]);
  const [searchInput, setSearchInput] = useState('');
  let [searchParams, setSearchParams] = useSearchParams();

  const getServices = async () => {
    const url = 'http://localhost:8080/api/appointments/'
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setServices(data.appointments);
    }
  }

  const handleSearchInput = (event) => {
    const value = event.target.value;
    setSearchInput(value);
}

  const executeSearch = (event) => {
    event.preventDefault();
    setSearchParams({filter: searchInput});
    }

  useEffect(() => {
    getServices();
}, []);

  return (
    <div>
      <h1>Service History</h1>
      <div>
        <input value={searchInput}
          onChange={handleSearchInput}
          name="VINsearch"
          id="VINsearch"
          placeholder="Search by VIN..."/>
      <button onClick={executeSearch} className="btn btn-secondary">Search</button>
    </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>VIP?</th>
            <th>Customer</th>
            <th>Booked</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {services?.filter((service) => {
            let filter = searchParams.get("filter");
            if(!filter) return true;
            let vin = service.vin.toLowerCase();
            return vin.startsWith(filter.toLowerCase());
          })
          .map((service, index) => {
            return (
              <tr key={service.vin + index}>
                <td>{service.vin}</td>
                <td>{service.vip ? 'Yes' : 'No'}</td>
                <td>{service.customer}</td>
                <td>{new Date(service.date_time).toLocaleDateString("en-US")} {new Date(service.date_time).toLocaleTimeString("en-US", { hour: "2-digit", minute: "2-digit" })}</td>
                <td>{service.technician.first_name} {service.technician.last_name}</td>
                <td>{service.reason}</td>
                <td>{service.status}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ServiceHistory;

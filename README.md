# CarCar

Team:

* Rich Thurman - Sales Microservice
* Kevin Ghiringhelli - Service Microservice


## Service microservice

The service microservice has three models:

-The Technician model includes the technician's first name, last name, and their employee ID

-The Appointment model includes the date and time the appointment was created, the reason for the appointment, the VIN of the car being serviced, the name of the customer, a VIP status which reflects whether or not the car was purchased from the dealership, the status of the appointment (canceled, created, or finished), and the name of the technician who will be completing the service.

-The AutomobileVO model, which is a value object of the automobiles in the inventory microservice.  It includes the VIN of the automobile.  There is a poller that polls the inventory microservice once a minute to pull any new automobiles and add them to the AutomobileVO database

## Sales microservice

The Sales microservice consist of service, a poller, and components for a car dealership. It includes the backend API services for inventory, sales, customers, and salespeople, as well as frontend React components for managing and displaying data.

## Step-by-step Instructions to Run the Project

1. Clone the repository in your desired folder using `git clone https://gitlab.com/kghiringhelli/project-beta`
2. Navigate to the project folder `cd project-beta` and run the following commands:

    -`docker volume create beta-data` to create a new volume for your containers to store data

    -`docker-compose build` to build a Docker image

    -`docker-compose up` to start the containers

3. In the docker logs for the `project-beta-react-1` container, once you see a message saying `Compiled successfully! You can now view app in the browser.` you will be able to view the application at http://localhost:3000/

## Diagram of the Project

![Project Diagram](project-diagram.png)

## Explicitly Defined URLs and Ports for Each Service

| Service         | URL                               | Port |
|-----------------|-----------------------------------|------|
| Inventory API   | http://localhost:8100/api/        | 8100 |
| Sales API       | http://localhost:8090/api/        | 8090 |
| Service API     | http://localhost:8080/api/        | 8080 |
| Frontend        | http://localhost:3000/            | 3000 |

## CRUD Route Documentation for Each Service

Using Insomnia (or another REST client), navigate to the following URLs with the corresponding HTTP method to view, send, delete, or edit data.  Example JSON bodies for POST requests are provided underneath each list.

For PUT requests where you want to edit data, first enter the corresponding URL in Insomnia with the correct ID or VIN for the item you want to update. Then, use a JSON body like the ones provided in the examples for POST requests, but only include the fields that you want to edit.

For DELETE requests, as well as the PUT requests in the Appointments API where you are only updating the status of the appointment, simply enter the corresponding URL in Insomnia, using the ID or VIN of the item you want to update, and send the request, and the item will be updated or deleted.



### Inventory API

##### Manufacturers

| Route                   | Method | Description                                  |
|-------------------------|--------|----------------------------------------------|
| http://localhost:8100/api/manufacturers/     | GET    | Get a list manufacturers	                  |
| http://localhost:8100/api/manufacturers/     | POST   | Add a new manufacturer                       |
| http://localhost:8100/api/manufacturers/:id/ | GET    | Get a specific manufacturer by ID            |
| http://localhost:8100/api/manufacturers/:id/ | PUT    | Update a specific manufacturer by ID         |
| http://localhost:8100/api/manufacturers/:id/ | DELETE | Delete a specific manufacturer by ID         |

<details>
    <summary>Add Manufacturer Input (POST)</summary>

    {
        "name": "Chrysler"
    }

A successful post will return the following in Insomnia:

    {
	    "href": "/api/manufacturers/2/",
	    "id": 2,
        "name": "Chrysler"
    }

</details>

##### Vehicle Models

| Route                   | Method | Description                                  |
|-------------------------|--------|----------------------------------------------|
| http://localhost:8100/api/models/            | GET    | Get a list models       	                  |
| http://localhost:8100/api/models/            | POST   | Add a new model        	                  |
| http://localhost:8100/api/models/:id/        | GET    | Get a specific model by ID	                  |
| http://localhost:8100/api/models/:id/        | PUT    | Update a specific model by ID                |
| http://localhost:8100/api/models/:id/        | DELETE | Delete a specific model by ID                |

<details>
    <summary>Add Model Input (POST)</summary>

    {
        "name": "Sebring",
        "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
        "manufacturer_id": 2
    }

A successful post will return the following in Insomnia:

    {
	    "href": "/api/models/2/",
	    "id": 2,
	    "name": "Sebring",
	    "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	    "manufacturer": {
		    "href": "/api/manufacturers/2/",
		    "id": 2,
		    "name": "Chrysler"
	    }
    }

</details>

##### Automobile Inventory

| Route                   | Method | Description                                  |
|-------------------------|--------|----------------------------------------------|
| http://localhost:8100/api/automobiles/       | GET    | Get a list of all automobiles                |
| http://localhost:8100/api/automobiles/       | POST   | Add a new automobile                         |
| http://localhost:8100/api/automobiles/:vin/   | GET    | Get a specific automobile by VIN              |
| http://localhost:8100/api/automobiles/:vin/   | PUT    | Update an automobile by VIN	                  |
| http://localhost:8100/api/automobiles/:vin/   | DELETE | Delete an automobile by VIN	                  |

<details>
    <summary>Add Automobile Input (POST)</summary>

    {
        "color": "red",
        "year": 2012,
        "vin": "1C3CC5FB2CN120174",
        "model_id": 1
    }

A successful post will return the following in Insomnia:

    {
        "href": "/api/automobiles/1C3CC5FB2CN120174/",
        "id": 3,
        "color": "red",
        "year": 2012,
        "vin": "1C3CC5FB2CN120174",
        "model": {
            "href": "/api/models/1/",
            "id": 1,
            "name": "Mustang",
            "picture_url": "https://cdn.jdpower.com/JDP_2024%20Ford%20Mustang%20Blue%20Front%20Quarter%20View%20Action.jpg",
            "manufacturer": {
                "href": "/api/manufacturers/1/",
                "id": 1,
                "name": "Ford"
            }
        },
        "sold": false
    }

</details>

### Sales API

| Route               | Method | Description                                  |
|---------------------|--------|----------------------------------------------|
| http://localhost:8090/api/sales/         | GET    | Get a list of all sales                      |
| http://localhost:8090/api/sales/         | POST   | Add a new sale                               |
| http://localhost:8090/api/sales/:id/     | GET    | Get a specific sale                          |
| http://localhost:8090/api/sales/:id/     | DELETE | Delete a specific sale                       |

<details>
    <summary>Add Sale Input (POST)</summary>

    {
	    "vin": "5TESN92NX4Z335831",
	    "price": "19995.00",
	    "customer_id": 2,
	    "salesperson_id": 1
    }

A successful post will return the following in Insomnia:

    {
	    "message": "Sale created successfully"
    }

</details>

### Customer API

| Route               | Method | Description                                  |
|---------------------|--------|----------------------------------------------|
| http://localhost:8090/api/customers/     | GET    | Get a list of all customers                  |
| http://localhost:8090/api/customers/     | POST   | Add a new customer                           |
| http://localhost:8090/api/customers/:id/ | GET    | Get a specific customer                      |
| http://localhost:8090/api/customers/:id/ | DELETE | Delete a specific customer                   |

<details>
    <summary>Add Customer Input (POST)</summary>

    {
		"first_name": "Fake",
		"last_name": "Customer",
		"address": "321 Fake Street"
    }

A successful post will return the following in Insomnia:

    {
	    "message": "Customer created successfully"
    }

</details>

### Salespeople API

| Route                 | Method | Description                                  |
|-----------------------|--------|----------------------------------------------|
| http://localhost:8090/api/salespeople/     | GET    | Get a list of all salespeople                |
| http://localhost:8090/api/salespeople/     | POST   | Add a new salesperson                        |
| http://localhost:8090/api/salespeople/:id/ | GET    | Get a specific salesperson                   |
| http://localhost:8090/api/salespeople/:id/ | DELETE | Delete a specific salesperson                |

<details>
    <summary>Add Salesperson Input (POST)</summary>

    {
        "first_name": "Sales",
        "last_name": "Person",
        "employee_id": "salesperson"
    }

A successful post will return the following in Insomnia:

    {
	    "message": "Salesperson created successfully"
    }

</details>

### Technicians API

| Route                 | Method | Description                                  |
|-----------------------|--------|----------------------------------------------|
| http://localhost:8080/api/technicians/     | GET    | Get a list of all technicians               |
| http://localhost:8080/api/technicians/     | POST   | Add a new technician                        |
| http://localhost:8080/api/technicians/:id/ | DELETE | Delete a specific technician                |

<details>
    <summary>Add Technician Input (POST)</summary>

    {
        "first_name": "John",
        "last_name": "Doe",
        "employee_id": "jdoe123"
    }

A successful post will return the following in Insomnia:

    {
	    "id": 2,
	    "first_name": "John",
	    "last_name": "Doe",
	    "employee_id": "jdoe123"
    }

</details>

### Appointments API

| Route                 | Method | Description                                  |
|-----------------------|--------|----------------------------------------------|
| http://localhost:8080/api/appointments/     | GET    | Get a list of all appointments               |
| http://localhost:8080/api/appointments/     | POST   | Add a new appointment                        |
| http://localhost:8080/api/appointments/:id/ | DELETE | Delete a specific appointment                |
|http://localhost:8080/api/appointments/:id/finish | PUT | Set an appointment's status to finished                |
| http://localhost:8080/api/appointments/:id/cancel | PUT | Set an appointment's status to canceled                |

<details>
    <summary>Add Appointment Input (POST)</summary>

    {
        "date_time": "2023-04-26T00:30:27+00:00",
        "reason": "Replace headlight fluid",
        "vin": "1FMEU15H2LLB00579",
        "customer": "John Doe",
        "technician_id": 1
    }

A successful post will return the following in Insomnia:

    {
        "href": "/api/appointments/5/",
        "id": 5,
        "date_time": "2023-04-26T00:30:27+00:00",
        "reason": "Replace headlight fluid",
        "vip": false,
        "status": "Created",
        "vin": "1FMEU15H2LLB00579",
        "customer": "John Doe",
        "technician": {
            "id": 1,
            "first_name": "Tech",
            "last_name": "Nichian",
            "employee_id": "technician"
	}

}
</details>

## Identification of the Value Objects

- AutomobileVO: Contains the information for an automobile, including its VIN, and HREF.

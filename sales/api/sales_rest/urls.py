from django.urls import path
from .import views

urlpatterns = [
    path('customers/', views.api_list_customers, name="api_list_customers"),
    path('customers/<int:customer_id>/', views.api_customer_details, name="api_customer_details"),
    path('salespeople/', views.api_list_salespeople, name="api_list_salespeople"),
    path('salespeople/<int:salesperson_id>/', views.api_salesperson_details, name="api_salesperson_details"),
    path('sales/', views.api_list_sales, name="api_list_sales"),
    path('sales/<int:sale_id>/', views.api_sale_details, name="api_sale_details"),


]

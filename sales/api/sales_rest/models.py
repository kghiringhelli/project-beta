from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):

    href = models.CharField(max_length=255, null=True)
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return f"{self.vin}"

class Salesperson(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    employee_id = models.CharField(max_length=255)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

class Customer(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    address = models.CharField(max_length=255)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales",
        on_delete=models.CASCADE,
        )
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    price = models.DecimalField(max_digits=1000,decimal_places=2)

    def __str__(self):
        return f"Sale: {self.id} - {self.automobile} - {self.salesperson} - {self.customer} - {self.price}"

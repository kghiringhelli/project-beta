import React, { useEffect, useState } from "react";
import withNotification from '../hocs/withNotification';


function CreateAutomobile({showNotification}) {
  const [models, setModels] = useState([]);
  const [color, setColor] = useState('');
  const [year, setYear] = useState('');
  const [vin, setVIN] = useState('');
  const [model, setModel] = useState('');

  const getModels = async () => {
    const url = 'http://localhost:8100/api/models/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    } else {
      showNotification('Error fetching models.', 'danger');
    }

  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      color: color,
      year: year,
      vin: vin,
      model_id: model,
    };

    const autoUrl = 'http://localhost:8100/api/automobiles/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
    },
    };

    try {

      const response = await fetch(autoUrl, fetchConfig);
      if (response.ok) {
        const newAuto = await response.json();
        setColor('');
        setYear('');
        setVIN('');
        setModel('');
        showNotification('Automobile created successfully!', 'success', '/automobiles');
      } else {
        showNotification(`Error creating Automobile`, 'danger');
      }

    } catch (error) {
      showNotification(`Error creating automobile: ${error}`, 'danger');
    }

  };

  const handleColorChange = (event) => {
    const value = event.target.value;
    setColor(value);
  }

  const handleYearChange = (event) => {
    const value = event.target.value;
    setYear(value);
  }

  const handleVINChange = (event) => {
    const value = event.target.value;
    setVIN(value);
  }

  const handleModelChange = (event) => {
    const value = event.target.value;
    setModel(value);
  }

  useEffect(() => {
    getModels();
}, []);

  return (
    <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add an automobile to inventory</h1>
                <form onSubmit={handleSubmit} id="create-auto-form">
                <div className="form-floating mb-3">
                    <input onChange={handleColorChange} value={color}placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                        <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleYearChange} value={year}placeholder="Year" required type="number" name="year" id="year" className="form-control" />
                        <label htmlFor="year">Year</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleVINChange} value={vin}placeholder="VIN" required type="text" name="VIN" id="VIN" className="form-control"/>
                        <label htmlFor="VIN">VIN</label>
                </div>
                <div className="mb-3">
                    <select
                    onChange={handleModelChange}
                    value={model}
                    required
                    name="model"
                    id="model"
                    className="form-select"
                    >
                    <option value="">Choose a model...</option>
                        {models.map(model => {
                            return (
                                <option key={model.id} value={model.id}>
                                    {model.name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create Automobile</button>
                </form>
            </div>
            </div>
        </div>
  );
}

export default withNotification(CreateAutomobile);

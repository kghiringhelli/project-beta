import React, { useEffect, useState } from 'react';
import withNotification from '../hocs/withNotification';



function CreateServiceAppointment({showNotification}) {
  const [technicians, setTechnicians] = useState([]);
  const [vin, setVin] = useState('');
  const [customer, setCustomer] = useState('');
  const [datetime, setDatetime] = useState('');
  const [technician, setTechnician] = useState('');
  const [reason, setReason] = useState('');

  const fetchTechnicians = async () => {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  }
  const handleSubmit = async (event) => {
    event.preventDefault();

    if (vin.length === 0 || vin.length > 17) {
      showNotification('VIN should not be empty or exceed 17 characters', 'danger');
      return;
    }

    if (customer.length === 0 || customer.length > 200) {
      showNotification('Customer should not be empty or exceed 200 characters', 'danger');
      return;
    }

    if(technician === '') {
      showNotification('Please select a technician', 'danger');
      return;
    }


    const data = {};
    data.vin = vin;
    data.customer = customer;
    data.date_time = datetime;
    data.technician_id = technician;
    data.reason = reason;

    const url = 'http://localhost:8080/api/appointments/'
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
          "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const newAppt = await response.json();
    setVin('');
    setCustomer('');
    setDatetime('');
    setReason('');
    setTechnician('');
    showNotification('Appointment created successfully!', 'success', '/service-appointments')
    } else {
      showNotification('Error creating appointment', 'danger');
    }
  };

  const handleVINChange = (event) => {
    const value = event.target.value;
    setVin(value);
  };
  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  };
  const handleDatetimeChange = (event) => {
    const value = event.target.value;
    setDatetime(value);
  };
  const handleReasonChange = (event) => {
    const value = event.target.value;
    setReason(value);
  };
  const handleTechnicianChange = (event) => {
    const value = event.target.value;
    setTechnician(value);
  };

  useEffect(() => {
    fetchTechnicians();
}, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Create a Service Appointment</h1>
        <form onSubmit={handleSubmit} id="create-appt-form">
          <div className="form-floating mb-3">
            <input onChange={handleVINChange} value={vin} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
              <label htmlFor="vin">VIN</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleCustomerChange} value={customer} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control" />
              <label htmlFor="customer">Customer</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleDatetimeChange} value={datetime} placeholder="Date/Time" required type="datetime-local" name="datetime" id="datetime" className="form-control" />
              <label htmlFor="datetime">Date/Time</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleReasonChange} value={reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
              <label htmlFor="reason">Reason</label>
          </div>
          <div className="form-floating mb-3">
            <select onChange={handleTechnicianChange} value={technician} placeholder="Technician" required type="text" name="technician" id="technician" className="form-select">
              <option value="">Choose a technician</option>
                {technicians?.map(technician => {
                  return (
                    <option key={technician.id} value={technician.id}>{technician.first_name} {technician.last_name}</option>
                  );
                })}
            </select>
          </div>
          <button className="btn btn-primary">Create Appointment</button>
        </form>
      </div>
      </div>
    </div>
  );
}

export default withNotification(CreateServiceAppointment);

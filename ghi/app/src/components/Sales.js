import React, { useState, useEffect } from 'react';

function Sales() {
  const[sales, setSales] = useState([]);
  const [filteredSales, setFilteredSales] = useState([]);
  const [salespersons, setSalespeople] = useState([]);


  const fetchSales = async () => {
    const url = 'http://localhost:8090/api/sales/';
    const response = await fetch(url);
    if(response.ok) {
      const data = await response.json();
      setSales(data.sales);
      setFilteredSales(data.sales);

    }
  }

  const fetchSalespeople = async () => {
    const url = 'http://localhost:8090/api/salespeople/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
    }
  };

  useEffect(() => {
    fetchSales();
    fetchSalespeople();
  }, []);

  const handleSalespeopleChange = (event) => {
    const salespersonId = parseInt(event.target.value);
    if (salespersonId === -1) {
      setFilteredSales(sales);
    } else {
      const filtered = sales.filter(
        (sale) => sale.salesperson.id === salespersonId
      );
      setFilteredSales(filtered);
    }
  };

  return (
    <div>
      <h1>Sales</h1>
      <hr />
      <div>
        <label htmlFor="salesperson">Filter by Salesperson: </label>
        <select
          id="salesperson"
          className="form-select"
          onChange={handleSalespeopleChange}
        >
          <option value={-1}>All Salespeople</option>
          {salespersons.map((salesperson) => (
            <option key={salesperson.id} value={salesperson.id}>
              {salesperson.first_name} {salesperson.last_name}
            </option>
          ))}
        </select>
      </div>
      <div className="table-responsive mt-3">
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Sale ID</th>
              <th>Price</th>
              <th>VIN</th>
              <th>Salesperson</th>
              <th>Customer</th>
            </tr>
          </thead>
          <tbody>
            {filteredSales.map((sale) => (
              <tr key={sale.id}>
                <td>{sale.id}</td>
                <td>{sale.price}</td>
                <td>{sale.vin}</td>
                <td>
                  {sale.salesperson.first_name} {sale.salesperson.last_name} (
                  Employee ID: {sale.salesperson.employee_id})
                </td>
                <td>
                  {sale.customer.first_name} {sale.customer.last_name} (Address:{' '}
                  {sale.customer.address})
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}


export default Sales;

import React, { useState } from 'react';
import withNotification from '../hocs/withNotification';

function AddCustomer({showNotification}) {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [address, setAddress] = useState('');

  const handleSubmit = async (event) => {
    event.preventDefault();

    //Validate Firstname
    if (firstName.length === 0 || firstName.length > 255) {
      showNotification('First name should not be empty or exceed 255 characters', 'danger');
      return;
    }

    //Validate Lastname
    if (lastName.length === 0 || lastName.length > 255) {
      showNotification('Last name should not be empty or exceed 255 characters', 'danger');
      return;
    }

    //Validate Address
    if (address.length === 0 || address.length > 255) {
      showNotification('Address should not be empty or exceed 255 characters', 'danger');
      return;
    }

    const data = {

      first_name: firstName,
      last_name: lastName,
      address: address,

    }
    const url = 'http://localhost:8090/api/customers/';
    const config = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    try{
      const response = await fetch(url, config);
      if (response.ok) {

        setFirstName('');
        setLastName('');
        setAddress('');
        showNotification('Customer added successfully!','success','/customers');
      } else {
        showNotification('Error adding customer','danger');
      }
    } catch (error) {
      showNotification(`Error creating manufacturer: ${error}`, 'danger');
    }
  };

  return (
    <div className="container">
      <h1>Add Customer</h1>
      <form onSubmit={handleSubmit}>
        <div className="row">
          <div className="col-md-6 mb-3">
            <label htmlFor="firstName">First Name</label>
            <input
              type="text"
              className="form-control"
              id="firstName"
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
            />
          </div>
          <div className="col-md-6 mb-3">
            <label htmlFor="lastName">Last Name</label>
            <input
              type="text"
              className="form-control"
              id="lastName"
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-12 mb-3">
            <label htmlFor="address">Address</label>
            <input
              type="text"
              className="form-control"
              id="address"
              value={address}
              onChange={(e) => setAddress(e.target.value)}
            />
          </div>
        </div>
        <button type="submit" className="btn btn-primary">
          Add Customer
        </button>
      </form>
    </div>
  );
}

export default withNotification(AddCustomer);

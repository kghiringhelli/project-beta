import React, { useState } from 'react';
import Notification from '../components/Notification';
import { useNavigate } from 'react-router-dom';


const withNotification = (WrappedComponent) => {
    return (props) => {
      const [notification, setNotification] = useState({ message: '', type: '', visible: false });
      const navigate = useNavigate();

      const showNotification = (message, type, redirectTo) => {
        setNotification({ message, type, visible: true });
        setTimeout(() => {
          setNotification({ message: '', type: '', visible: false });
          if(redirectTo) {
            navigate(redirectTo);
          }
        }, 2000);
      };

      return (
        <>
          {notification.visible && <Notification message={notification.message} type={notification.type} />}
          <WrappedComponent {...props} showNotification={showNotification} />
        </>
      );
    };
  };


export default withNotification;

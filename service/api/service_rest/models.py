from django.db import models
from django.urls import reverse

# Create your models here.

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return self.vin



class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=255)

    def __str__(self):
        return self.first_name + ' ' + self.last_name

class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.TextField()
    CREATED = "Created"
    CANCELED = "Canceled"
    FINISHED = "Finished"
    STATUS_CHOICES = ((CREATED, "Created"), (CANCELED, "Canceled"), (FINISHED, "Finished"))
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default=CREATED)
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=200)
    vip = models.BooleanField(default=False, null=True, blank=True)
    technician = models.ForeignKey(
        Technician,
        related_name="appointment",
        null=True,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.customer

    def get_api_url(self):
        return reverse("delete_appointment", kwargs={"id": self.id})
